%define nightly @NIGHTLY_VERSION@%{nil}
%define _prefix /usr
%define _mandir %{_prefix}/share/man
%define _sysconfdir %{_prefix}/etc
%define release 1

%if "%{?nightly:%{nightly}}%{!?nightly:0}" == "%{nil}"
%undefine nightly
%endif

Name: @PACKAGE@
Version: @BASE_VERSION@
Release: %{?nightly:0.%{nightly}}%{!?nightly:%{release}}%{?dist}
Summary: LSC Algorithm Stochastic Library
License: GPLv2+
Group: LAL
Source: %{name}-%{version}%{?nightly:-%{nightly}}.tar.xz
URL: https://wiki.ligo.org/DASWG/LALSuite
Packager: Adam Mercer <adam.mercer@ligo.org>
BuildRequires: python2-rpm-macros
BuildRequires: libmetaio-devel
BuildRequires: numpy
BuildRequires: octave-devel
BuildRequires: python-devel
BuildRequires: swig >= 3.0.7
BuildRequires: lal-devel >= @MIN_LAL_VERSION@
BuildRequires: lal-octave >= @MIN_LAL_VERSION@
BuildRequires: python2-lal >= @MIN_LAL_VERSION@
BuildRequires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
BuildRequires: lalmetaio-octave >= @MIN_LALMETAIO_VERSION@
BuildRequires: python2-lalmetaio >= @MIN_LALMETAIO_VERSION@
Requires: libmetaio
Requires: lal >= @MIN_LAL_VERSION@
Requires: lalmetaio >= @MIN_LALMETAIO_VERSION@
Prefix: %{_prefix}

%description
The LSC Algorithm Stochastic Library for gravitational wave data analysis.
This package contains the shared-object libraries needed to run applications
that use the LAL Stochastic library.

%package devel
Summary: Files and documentation needed for compiling programs that use LAL Stochastic
Group: LAL
Requires: %{name} = %{version}
Requires: libmetaio-devel
Requires: lal-devel >= @MIN_LAL_VERSION@
Requires: lalmetaio-devel >= @MIN_LALMETAIO_VERSION@
%description devel
The LSC Algorithm Stochastic Library for gravitational wave data analysis.
This package contains files needed build applications that use the LAL
Stochastic library.

%package -n python2-%{name}
Summary: Python %{python2_version} Bindings for LALStochastic
Group: LAL
Provides: %{name}-python
Obsoletes: %{name}-python < 1.1.20-1
Conflicts: %{name}-python
Requires: %{name} = %{version}
Requires: numpy
Requires: python
Requires: python2-lal >= @MIN_LAL_VERSION@
Requires: python2-lalmetaio >= @MIN_LALMETAIO_VERSION@
%description -n python2-%{name}
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Python %{python2_version} bindings for LALFrame.

%package octave
Summary: Octave Bindings for LALStochastic
Group: LAL
Requires: %{name} = %{version}
Requires: octave
Requires: lal-octave >= @MIN_LAL_VERSION@
Requires: lalmetaio-octave >= @MIN_LALMETAIO_VERSION@
%description octave
The LSC Algorithm Library for gravitational wave data analysis.
This package provides the Octave bindings for LALStochastic.

%prep
%setup -q -n %{name}-%{version}%{?nightly:-%{nightly}}

%check
%{__make} %{?_smp_mflags} V=1 VERBOSE=1 check

%build
%configure --disable-gcc-flags --enable-swig
%{__make} %{?_smp_mflags} V=1

%install
%make_install
find $RPM_BUILD_ROOT%{_libdir} -name '*.la' -delete

%post
ldconfig

%postun
ldconfig

%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}%{?nightly:-%{nightly}}

%files
%defattr(-,root,root)
%license COPYING
%{_bindir}/lalstochastic_version
%{_libdir}/*.so.*
%{_sysconfdir}/*

%files devel
%defattr(-,root,root)
%license COPYING
%{_includedir}/lal
%{_libdir}/*.a
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%files -n python2-%{name}
%defattr(-,root,root)
%license COPYING
%{python2_sitearch}/*

%files octave
%defattr(-,root,root)
%license COPYING
%{_prefix}/lib*/octave/*/site/oct/*/lalstochastic.oct*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Tue Feb 07 2017 Adam Mercer <adam.mercer@ligo.org> 1.1.20-1
- O2 release

* Mon Sep 26 2016 Adam Mercer <adam.mercer@ligo.org> 1.1.19-1
- ER10 release

* Thu Jun 23 2016 Adam Mercer <adam.mercer@ligo.org> 1.1.18-1
- ER9 release

* Fri Mar 25 2016 Adam Mercer <adam.mercer@ligo.org> 1.1.17-1
- Pre O2 packaging test release
